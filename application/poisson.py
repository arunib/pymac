#!/usr/bin/python3.2 -tt
# -*- coding: UTF-8 -*-
""" CSMA MPR simulation """

__version__ = "$Revision: 1.0 $"
# $Source$

__author__ = "Arun I B <arunib@smail.iitm.ac.in>"
__copyright__ = "Copyright (C) 2011 Arun I B"
#__license__     = TBD


from SimPy.Simulation import (Process, hold)

import random


class Application(Process):
    """ #TODO Exponential on off arrival process """

    def __init__(self, node):
        """ per node """
        Process.__init__(self, name="Application", sim=node.sim)
        self.node = node

    def application(self):
        """ main thread """
        while True:
            interArrivalTime = random.expovariate(float(1) /
                                    self.node.arrivalTime)
            #print "InterArrivalTime= %f" % interArrivalTime
            yield hold, self, interArrivalTime
            self.node.pkttosend = self.node.pkttosend + 1
            if self.node.pkttosend <= 0:
                print("nodeId=%d pkttosend=%d" % (
                self.node.NodeId, self.node.pkttosend))
