#!/usr/bin/python3.2 -tt
# -*- coding: UTF-8 -*-
""" CSMA MPR simulation """

__version__ = "$Revision: 1.1 $"
# $Source$

__author__ = "Arun I B <arunib@smail.iitm.ac.in>"
__copyright__ = "Copyright (C) 2012 Arun I B"
#__license__     = TBD


INFINITY = 10000000
from SimPy.Simulation import (SimEvent, Process, Resource, waitevent, Monitor,
now, hold, request, release, PriorityQ)
import math
import pylab as p


class MPRChannel(Process):
    """#Channel"""

    class Uplink(Process):
        """ to be used inside channel """

        def __init__(self, channel):
            """ subscribe to events """
            Process.__init__(self, name="uplink", sim=channel.sim)
            self.channel = channel
            self.Tx_Start_Event = SimEvent("TX Start Event", sim=self.sim)
            self.Tx_End_Event = SimEvent("TX End Event", sim=self.sim)
            # Idle Event 0-> When there are no transmissions
            self.Ch_Idle_Event0 = SimEvent("Channel Idle Event0", sim=self.sim)
            # Idle Event 1-> When number of transmissions is less than K
            self.Ch_Idle_EventK = SimEvent("Channel Idle EventK", sim=self.sim)
            self.sim.activate(self, self.uplink())

        def uplink(self):
            """ keep track of Nct """
            while True:
                yield request, self, self.channel.mutex, 0
                if(self.channel.ntx < self.channel.K):
                    self.Ch_Idle_EventK.signal()
                if(self.channel.ntx == 0):
                    self.Ch_Idle_Event0.signal()
                yield release, self, self.channel.mutex, 0
                yield waitevent, self, (self.Tx_End_Event)
                assert(len(self.eventsFired) == 1)
#                if(self.eventsFired[0] == self.Tx_Start_Event):
#                    self.channel.Nct = self.channel.Nct + 1
#                else:
#                    self.channel.Nct = self.channel.Nct - 1

    class Plotter(Process):
        def plotQ(self):
            moni = Monitor(sim=self.sim)
            p.ion()
            tstep = 0.1
            while self.sim.now() < self.sim.maxTime:
                yield hold, self, tstep
                moni.observe(self.channel.Nct)
                tAverage = moni.timeAverage()
                print("tAverage", tAverage)
                #if(now()>0):
                #print("Nct=", self.channel.Nct,self.sim.now())
                p.plot(moni.tseries(), moni.yseries(), "ro", [self.sim.now()],
                    [tAverage], "b.")
                p.ylabel("Number of concurrent transmissions")
                p.xlabel("time (s)")
                p.title("Number of transmissions at = %s seconds" %
                self.sim.now())
                p.draw()

        def __init__(self, channel):
            """ subscribe to events """
            Process.__init__(self, sim=channel.sim)
            self.channel = channel
            self.sim.activate(self, self.plotQ())

    def __init__(self, sim, channelparams):
        """ Initialize with Capacity and MPR capability """
        Process.__init__(self, sim=sim)
        self.K = channelparams["mpr"]
        self.name = "mprchannel" + str(self.K)
        self.C = channelparams["bitrate"] * math.pow(10, 6)  # bps
        #self.Nct = 0
        self.difs = channelparams["difs"]  # 128 microseconds
        self.subchannel = Resource(capacity=INFINITY, name='beam',
                                   monitored=True, sim=sim)
        self.mutex = Resource(capacity=1, name='mutex', qType=PriorityQ,
                                   preemptable=False, monitored=False, sim=sim)
        self.slot = channelparams["slot"]  # Slot(self.C)
        self.uplink = self.Uplink(self)
        #self.plotter = self.Plotter(self)
        return

    @property
    def ntx(self):
        """Get # of transmissions on the channel"""
        return INFINITY - self.subchannel.n

    def isBusy(self):
        """ checks if the channel is busy """
        if self.ntx == 0:
            return False
        else:
            return True

    def isBusyK(self):
        """ checks if the channel is busy """
        if self.ntx < self.K:
            return False
        else:
            return True

    def updateStatistics(self):
        """ return results """
        return self.subchannel.actMon.timeAverage()
